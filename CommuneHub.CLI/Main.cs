using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TokenStore.Bytelot;
using TokenStore.Util;

namespace CommuneHub.CLI
{
    class MainClass
    {

        public static void Main(string[] args)
        {
            var ms = new MemoryStream();
            var bl = new MarkedBytelot(ms);

            bl.Write(SerializationUtil.ULongToCompactBytes(ulong.MaxValue));
            bl.Write(Encoding.ASCII.GetBytes("Look"));
            bl.Write(Encoding.UTF8.GetBytes("+ěšč"));

            Console.WriteLine("{0}/{1}", ms.Position, ms.Length);

            bl.Read(chunk =>
            {
                Console.WriteLine("Chunk {0}:", chunk.BytelotSequence);
                Console.WriteLine(BinaryString(chunk.Open()));
                return true;
            });

            Console.WriteLine("{0}/{1}", ms.Position, ms.Length);
            Console.ReadLine();
        }

        static String BinaryString(Stream stream)
        {
            var sb = new StringBuilder((int)stream.Length * 9);
            var bitcount = 0;
            int i;
            while (0 <= (i = stream.ReadByte()))
            {
                foreach (bool bit in new BitArray(new byte[]{(byte)i}).Cast<bool>().Reverse())
                {
                    sb.Append(true.Equals(bit) ? '1' : '0');
                    if (++bitcount % 8 == 0) sb.Append(' ');
                    if (bitcount % 64 == 0) sb.Append('\n');
                }
            }

            return sb.ToString();
        }

        static String BinaryString(byte[] bytes)
        {
            var sb = new StringBuilder(bytes.Length * 9);
            var bitcount = 0;
            foreach (bool bit in new BitArray(bytes.Reverse().ToArray()).Cast<bool>().Reverse())
            {
                sb.Append(true.Equals(bit) ? '1' : '0');
                if (++bitcount % 8 == 0) sb.Append(' ');
                if (bitcount % 64 == 0) sb.Append('\n');
            }

            return sb.ToString();
        }

        static byte[] NumberToBytes(ulong num)
        {
            var bytelist = new List<byte>();
            while (num != 0)
            {
                bytelist.Insert(0, (byte)((num << 56) >> 56));
                num = num >> 8;
            }
            return bytelist.ToArray();
        }

        public static void Mainx(string[] args)
        {
            var number = ulong.Parse(args.FirstOrDefault() ?? "255");

            var bitcount = (int)Math.Ceiling(Math.Log(number + 1, 2));
            var bytecount = 1 + ((bitcount - 1) >> 3);
            var cbitcount = bitcount + bytecount;
            var cbytecount = 1 + ((cbitcount - 1) >> 3);

            Console.WriteLine("bits: {0}, bytes: {1}, cbits: {2}, cbytes: {3}", bitcount, bytecount, cbitcount, cbytecount);
            Console.WriteLine("Original: {0}", number);
            Console.WriteLine("Log2: {0}", Math.Log(number + 1, 2));

            var cbytes = new byte[cbytecount];

            for (var byteidx = 0; byteidx < cbytecount; byteidx++)
            {
                var lshift = (8 - cbytecount + byteidx);

                Console.WriteLine("idx: {0}, lshift: {1}, byte: {2}", byteidx, lshift, ((number << lshift) >> 56));

                cbytes[byteidx] = (byte)((number << (8 * lshift)) >> 56);
            }

            foreach (var b in cbytes)
                Console.WriteLine("Byte: {0}", b);

        }

        public static void Main3(string[] args)
        {
            string input;
            while (!"quit".StartsWith((input = Console.ReadLine()).ToLower()))
            {
                ulong x = ulong.Parse(String.IsNullOrWhiteSpace(input) ? args.FirstOrDefault() ?? ulong.MaxValue.ToString() : input);

                Console.WriteLine("Original: {0}", x);
                Console.WriteLine("Original: {0}", BinaryString(NumberToBytes(x)));

                byte[] b = x.ULongToCompactBytes();

                Console.WriteLine("Compact: {0}", BinaryString(b));

                Console.WriteLine(BitConverter.ToString(b));
                Console.WriteLine("Compact bytes: {0}", b[0].CompactBytesLength());

                var x2 = b.ULongFromCompactBytes();
                Console.WriteLine("Got: {0}, {1}", BinaryString(NumberToBytes(x2)), x2);
            }
        }
        public static void Main2(string[] args)
        {
            var uri = new Uri(args.FirstOrDefault() ?? "simple://sample");

            Console.WriteLine("Store URI: {0}", uri);

            var store = StoreLocator.Default.Open(uri);

            foreach (var msg in store.ListMessages())
            {
                Console.WriteLine(msg);
            }
        }
    }
}
