using System;
using System.Linq;

namespace CommuneHub.Common
{	
		public struct NameAndVersion
		{
			public NameAndVersion(params string[] parts)
			: this()
			{
				Name = parts.FirstOrDefault();
				Version = String.Join(".", parts.Skip(1));
			}
		
			public NameAndVersion(string dotNameAndVersion)
			: this(dotNameAndVersion.Split(new char[]{'.'},2))
			{
			}
			
			public String Name{get;set;}
			public String Version{get;set;}
						
			public override string ToString ()
			{
				return string.Format ("{0}.{1}", Name, Version);
			}
		}
}

