
using System;
using System.Collections.Generic;
using CommuneHub.Items;

namespace CommuneHub.Messages {
	public interface IMessage 
	{
		String UniqueId {get;}	
		IMessageReference Reference {get;}
		IEnumerable<IItem> Items {get;}
	}
}

