using System;

namespace CommuneHub.Messages
{
	public interface IMessageReference
	{
		String UniqueId {get;}
		String Description {get;}
	}
}

