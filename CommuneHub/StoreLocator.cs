using System;
using System.Linq;
using System.Collections.Generic;
using CommuneHub.Stores;
using CommuneHub.Stores.Simple;

namespace CommuneHub
{
	public class StoreLocator : IStoreFactory
	{		
		public static IStoreFactory Default = new StoreLocator(new SimpleStoreFactory());
		
		public readonly IList<IStoreFactory> Factories;
		
		public StoreLocator (params IStoreFactory[] factories)
		{
			Factories = factories.ToList();
		}
		
		#region IStoreFactory implementation
		public IStore Open (Uri storeUri, bool throwerr)
		{
			var store = Factories
				.Select(f=>f.Open(storeUri, throwerr))
				.Where(s=>s!=null)
				.FirstOrDefault()
				;
			
			if(store == null && throwerr)
				throw new ArgumentOutOfRangeException("storeUri", "no store was found for that URI");
			
			return store;
		}
		#endregion
	}
}

