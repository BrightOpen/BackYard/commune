using System;
using System.Linq;
using System.Collections.Generic;
using CommuneHub.Features.List;
using CommuneHub.Messages;

namespace CommuneHub.Stores.Simple
{
	public class SimpleListFeature : IListFeature
	{
		public SimpleListFeature (IDictionary<IMessageReference,IMessage> messages)
		{
			Messages = messages;
			Description = "List all messages";
		}		

		protected virtual IDictionary<IMessageReference,IMessage> Messages {get;set;}
		
		#region IListFeature implementation
		
		public IListReference OpenList ()
		{
			return null;
		}
		
		public virtual IEnumerable<IMessageReference> ListMessages (ref IListReference listReference)
		{
			if(listReference == null)
			{
				listReference = new SimpleListReference();
				return Messages.Keys.ToArray();
			}
			else
			{
				return new IMessageReference[0];
			}
		}

		public virtual IMessage GetMessage (IMessageReference messageReference)
		{
			return Messages[messageReference];
		}
		#endregion

		#region IFeature implementation
		public string Description {
			get ; protected set;
		}
		#endregion



	}
}

