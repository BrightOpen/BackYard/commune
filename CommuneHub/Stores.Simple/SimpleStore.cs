using System;
using System.Linq;
using System.Collections.Generic;
using CommuneHub.Items.Base;
using CommuneHub.Items;
using CommuneHub.Messages;
using CommuneHub.Features;

namespace CommuneHub.Stores.Simple
{
	public class SimpleStore : IStore
	{		
		
		public SimpleStore (IEnumerable<IMessage> messages = null, IItemHandler itemHandler = null)
		{
			ItemHandler = AcceptItemHandler(itemHandler);
			Messages = AcceptMessages(messages);
			Features = new IFeature[]{new SimpleListFeature(Messages)};
		}	

		#region IStore implementation
		public IEnumerable<IFeature> Features {
			get ; protected set;
		}
		#endregion	

		protected virtual IItemHandler ItemHandler {get;set;}
		protected virtual IDictionary<IMessageReference,IMessage> Messages {get;set;}
		
		protected virtual IItemHandler AcceptItemHandler(IItemHandler itemHandler)
		{
			return itemHandler ?? new CompositeItemHandler() {
				Handlers = new IItemHandler[]
				{
					new BaseItemHandler()
				}
			};
		}
		
		protected virtual Dictionary<IMessageReference,IMessage> AcceptMessages(IEnumerable<IMessage> messages)
		{			
			return (messages ?? new IMessage[0])
					.Select(m=>new SimpleMessage(m) as IMessage)
					.ToDictionary(m=> m.Reference);			
		}
	
		public static IEnumerable<IMessage> GetSampleMessages()
		{
			return new SimpleMessage[]{
						new SimpleMessage()
						{
							Items = new IItem[]{
								new TitleItem(){Title = "Message 1"}
							}
						},
						new SimpleMessage()
						{
							Items = new IItem[]{
								new TitleItem(){Title = "Message 2"},
								new GenericItem(){Data = "Secret"}
							}
						},
						new SimpleMessage()
						{
							
						},
						new SimpleMessage()
						{
							
						},					
					};
		}
	}
}

