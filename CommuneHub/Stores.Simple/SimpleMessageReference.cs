using System;
using CommuneHub.Messages;

namespace CommuneHub.Stores.Simple
{
	public class SimpleMessageReference : IMessageReference
	{
		public SimpleMessageReference ()
		{
		}

		#region IMessageReference implementation
		public virtual string UniqueId { get ; set; }

		public virtual string Description { get ; set; }
		#endregion
		
		public override string ToString ()
		{
			return String.Format ("MsgRef {0}", Description);
		}
	}
}

