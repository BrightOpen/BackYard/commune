using System;
using System.Collections.Generic;
using CommuneHub.Messages;

namespace CommuneHub.Stores.Simple
{
	public class SimpleStoreFactory : IStoreFactory
	{
		public SimpleStoreFactory ()
		{
		}

		#region IStoreFactory implementation
		public IStore Open (Uri storeUri, bool throwerr)
		{
			if(storeUri.Scheme=="simple")
			{
				if(storeUri.AbsolutePath == "/sample")
					return new SimpleStore(SimpleStore.GetSampleMessages());
				else if(storeUri.Host == "sample")
					return new SimpleStore(SimpleStore.GetSampleMessages());
				else
					return new SimpleStore();
			}
			else
			{
				return null;
			}
		}
		#endregion
	}
}

