using System;
using System.Linq;
using System.Collections.Generic;
using CommuneHub.Items;
using CommuneHub.Messages;

namespace CommuneHub.Stores.Simple
{
	public class SimpleMessage : IMessage
	{
		protected IList<IItem> ItemsList = new IItem[0];
		
		public SimpleMessage (IMessage copyMessage = null, IItemHandler itemHandler = null)
		{
			if(copyMessage!=null)
			{
				ItemsList = new List<IItem>();
				
				foreach(var item in copyMessage.Items)
				{
					if(itemHandler == null)
					{
						ItemsList.Add(item);
					}
					else
					{
						var itemOwned = itemHandler.Create(item.Framework, item.Type, item.Name, item.Relevance,item.Data);
						ItemsList.Add(itemOwned ?? item);
					}
				}
				
				UniqueId = copyMessage.UniqueId;				
			}
			else
			{
				UniqueId = Guid.NewGuid().ToString();
			}
			
			Reference = new SimpleMessageReference()
			{
				Description = this.ToString(),
				UniqueId = UniqueId
			};
		}
		
		public override string ToString ()
		{			
			var items = String.Join(", ", ItemsList.Select(i=>Convert.ToString(i)));
			
			return string.Format ("{0} with {1} items: {2}", UniqueId, ItemsList.Count, items);
		}
		
		#region IMessage implementation
		public virtual IMessageReference Reference { get ; set; }
		
		public virtual string UniqueId { get ; set; }

		public virtual IEnumerable<IItem> Items {
			get {
				return ItemsList.ToArray();
			}
			set {
				ItemsList = value.ToArray();
			}
		}
		#endregion
	}
}

