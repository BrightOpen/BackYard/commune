using System;
using System.Collections.Generic;
using CommuneHub.Messages;

namespace CommuneHub.Features.List
{
	public interface IListFeature : IFeature
	{
		IListReference OpenList ();		
		IEnumerable<IMessageReference> ListMessages(ref IListReference listReference);
		IMessage GetMessage (IMessageReference messageReference);
	}
}

