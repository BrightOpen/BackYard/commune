using System;

namespace CommuneHub.Items
{
	public interface IItemHandler
	{
		IItem Create (string framework, string type, string name, string relevance, object data);
		IItem Create (Common.NameAndVersion framework, Common.NameAndVersion type, string name, string relevance, object data);
	}
}

