using System;
using System.Collections.Generic;

namespace CommuneHub.Items
{
	public class CompositeItemHandler : GenericItemHandler
	{
		public IList<IItemHandler> Handlers = new List<IItemHandler> ();
		
		public CompositeItemHandler (bool createDefault = true) : base(createDefault)
		{
			
		}
		
		#region IItemHandler implementation
		
		public override IItem Create (Common.NameAndVersion framework, Common.NameAndVersion type, string name, string relevance, object data)
		{
			if(Handlers!=null)
			{
				foreach (var handler in Handlers) {
					var item = handler.Create (framework, type, name, relevance, data);
					if (item != null)
						return item;
				}
			}
			
			return base.Create (framework, type, name, relevance, data);
		}
		#endregion
		
		
	}
}

