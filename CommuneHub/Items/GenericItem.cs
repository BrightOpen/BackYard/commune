using System;

namespace CommuneHub.Items
{
	public class GenericItem : IItem
	{		
		public GenericItem()
		{
			Init();
		}
		
		#region IMessageItem implementation
		public virtual string Name {get; set;}
		public virtual string Type {get; set;}
		public virtual string Framework {get; set;}
		public virtual string Relevance {get; set;}
		public virtual object Data {get; set;}
		#endregion
		
		protected virtual void Init()
		{
			Name = "generic";
			Type = "generic";
			Framework = "basic.1.1";
			Relevance = "essential";
			Data = "";
		}
		
		public override string ToString ()
		{
			return string.Format ("[GenericItem: Name={0}, Type={1}, Framework={2}, Relevance={3}, Data={4}]", Name, Type, Framework, Relevance, Data);
		}
	}
}

