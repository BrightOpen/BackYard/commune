
using System;

namespace CommuneHub.Items {
	public interface IItem
	{
		String Name {get;}
		String Type {get;}
		String Framework {get;}
		String Relevance {get;}
		Object Data {get;}
	}
}

