using System;

namespace CommuneHub.Items
{
	public class GenericItemHandler : IItemHandler
	{
		public bool CreateDefault { get; protected set; }
		
		public GenericItemHandler(bool createDefault = false)
		{
			CreateDefault = createDefault;
		}
		
		#region IItemHandler implementation
		public virtual IItem Create (string framework, string type, string name, string relevance, object data)
		{
			if (framework == null)
				throw new ArgumentNullException ("framework");
			if (type == null)
				throw new ArgumentNullException ("type");
			if (name == null)
				throw new ArgumentNullException ("name");
			if (relevance == null)
				throw new ArgumentNullException ("relevance");
			if (data == null)
				throw new ArgumentNullException ("data");
			return Create (framework.GetNameAndVersion (), type.GetNameAndVersion (), name, relevance, data);
		}

		public virtual IItem Create (Common.NameAndVersion framework, Common.NameAndVersion type, string name, string relevance, object data)
		{			
			if (String.IsNullOrEmpty (framework.Name))
				throw new ArgumentNullException ("framework");
			if (String.IsNullOrEmpty (type.Name))
				throw new ArgumentNullException ("type");
			if (name == null)
				throw new ArgumentNullException ("name");
			if (relevance == null)
				throw new ArgumentNullException ("relevance");
			if (data == null)
				throw new ArgumentNullException ("data");
			
			if (CreateDefault) {
				return new GenericItem ()
				{
					Name = name,
					Type = Convert.ToString (type),
					Framework = Convert.ToString (framework),
					Relevance = relevance,
					Data = data
				};
			} else {
				return null;
			}
		}
		#endregion
	}
}

