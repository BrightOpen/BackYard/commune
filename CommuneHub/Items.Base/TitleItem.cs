using System;

namespace CommuneHub.Items.Base
{
	public class TitleItem : GenericItem
	{
		public String Title{
			get{return Convert.ToString(Data);}
			set{Data = value;}
		}
		
		protected override void Init ()
		{
			base.Init ();
			Name = "title";
			Type = "title";
		}
		
		public override string ToString ()
		{
			return string.Format ("[TitleItem: Title={0}]", Title);
		}
	}
}

