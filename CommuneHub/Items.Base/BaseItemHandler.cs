using System;

namespace CommuneHub.Items.Base
{
	public class BaseItemHandler : GenericItemHandler
	{
		public override IItem Create (Common.NameAndVersion framework, Common.NameAndVersion type, string name, string relevance, object data)
		{
			switch (framework.Name) {
			case "base":
				break;
			default:
				break;
			}
			
			return base.Create (framework, type, name, relevance, data);
		}
	}
}

