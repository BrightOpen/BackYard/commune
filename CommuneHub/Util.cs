using System;
using System.Linq;
using System.Collections.Generic;
using CommuneHub.Stores;
using CommuneHub.Messages;
using CommuneHub.Features.List;

namespace CommuneHub
{
	public static class Util
	{
		public static Common.NameAndVersion GetNameAndVersion(this String dotNameAndVersion)
		{
			return new Common.NameAndVersion(dotNameAndVersion);
		}	
		
		public static T GetFeature<T>(this IStore store)
		{
			if(store == null) throw new ArgumentNullException("store");
			return store.Features.OfType<T>().FirstOrDefault();
		}
		
		public static IEnumerable<IMessage> ListMessages(this IStore store)
		{
			if(store == null) throw new ArgumentNullException("store");
			
			var listfeature = store.GetFeature<IListFeature>();
			if(listfeature == null) throw new InvalidOperationException("Store does not support a list feature");			
			
			var listref = listfeature.OpenList();
			var done = false;
			while(!done)
			{
				done = true;
				foreach(var msgref in listfeature.ListMessages(ref listref))
				{
					done = false;					
					yield return listfeature.GetMessage(msgref);
				}
			}
		}
	}
}

