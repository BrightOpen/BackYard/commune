using System;

namespace CommuneHub.Stores
{
	public interface IStoreFactory
	{
		IStore Open (Uri storeUri, bool throwerr = true);
	}
}

