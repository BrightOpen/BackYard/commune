using System.Collections.Generic;
using CommuneHub.Features;

namespace CommuneHub.Stores
{
	public interface IStore
	{
		IEnumerable<IFeature> Features {get;}
	}
}

