using System;

namespace TokenStore
{
	public interface ITokenProcessor
	{
		void Process<T>(T token) where T:IToken;
	}
}

