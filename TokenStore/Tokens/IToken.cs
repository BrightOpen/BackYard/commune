using System;

namespace TokenStore
{
	public interface IToken
	{
		Object Data {get;}
		String Type {get;}
	}
}

