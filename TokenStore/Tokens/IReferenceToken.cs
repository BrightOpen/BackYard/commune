using System;

namespace TokenStore
{
	public interface IReferenceToken : IToken
	{
		String UniqueId {get;}
	}
}

