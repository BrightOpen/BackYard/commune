using System;

namespace TokenStore
{
	public class AtomStringToken : IToken
	{
		const string TypeName = "atom";
		
		public AtomStringToken (String atom)
		{
			Data = atom;
		}

		#region IToken implementation
		public object Data { get ; private set; }

		public string Type { get { return TypeName; } }
		#endregion
	}
}

