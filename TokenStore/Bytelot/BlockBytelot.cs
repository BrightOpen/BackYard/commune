﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TokenStore.Util;

namespace TokenStore.Bytelot
{
    public class BlockBytelot : IBytelot
    {
        private Stream stream;
        private int blocksize;

        public BlockBytelot(Stream stream, int blocksize = 0)
        {
            this.stream = stream;
            this.blocksize = blocksize <= 0 ? 4 * 1024 : blocksize;
        }

        public void Read(Func<IBytelotChunk, bool> processor = null)
        {
            if (!stream.CanRead) throw new InvalidOperationException("Stream is not readable");

            var buffer = new byte[blocksize];
            var bytes = new byte[0];

            do
            {
                lock (stream)
                {
                    bytes = stream.Read(buffer.Length);
                }
            }
            while (bytes.Length != 0 && processor != null && !processor(new BlockBytelotChunk(bytes, "", stream.Position.ToString())));
        }

        public IBytelotChunk Write(byte[] data = null)
        {
            if (!stream.CanWrite) throw new InvalidOperationException("Stream is not writable");
            if (!stream.CanSeek) throw new InvalidOperationException("Stream is not seekable");
            if (data == null) return null;

            lock (stream)
            {
                var chunk = new BlockBytelotChunk(data, "", stream.Length.ToString());
                stream.Write(data, dstSeek: stream.Length, dstRewind: true, buffersize:blocksize);
                return chunk;
            }
        }
    }
}
