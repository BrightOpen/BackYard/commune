﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TokenStore.Bytelot
{
    public interface IBytelot
    {
        void Read(Func<IBytelotChunk, bool> processor = null);
        IBytelotChunk Write(byte[] data = null);
    }
}
