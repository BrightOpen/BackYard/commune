﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TokenStore.Bytelot
{
    public interface IBytelotChunk
    {
        String BytelotId { get; }
        String BytelotSequence { get; }
        Stream Open();
    }
}
