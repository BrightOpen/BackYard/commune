﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TokenStore.Bytelot
{
    class BlockBytelotChunk : IBytelotChunk
    {
        public BlockBytelotChunk(byte[] data, string id, string seq)
        {
            Data = data ?? new byte[0];
            BytelotId = id;
            BytelotSequence = seq;
        }

        public string BytelotId
        {
            get;
            set;
        }

        public string BytelotSequence
        {
            get;
            set;
        }

        public byte[] Data
        {
            get;
            set;
        }

        public System.IO.Stream Open()
        {
            return new MemoryStream(Data, false);
        }
    }
}
