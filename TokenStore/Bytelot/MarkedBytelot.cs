﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TokenStore.Util;

namespace TokenStore.Bytelot
{
    public class MarkedBytelot : IBytelot
    {
        private Stream stream;
        private MemoryStream bookmarkStream;
        private MemoryStream dataStream;
        private Header header;
        private Queue<uint> bookmarks;
        private int blocksize;
        private ulong writeCount;
        private ulong readCount;
        private bool writeInitialized;
        private bool dirty;

        public MarkedBytelot(Stream stream, int blocksize = 0)
        {
            this.blocksize = blocksize >= 64
                                ? blocksize
                                : Streams.DefaultBufferSize >= 64
                                ? Streams.DefaultBufferSize
                                : 64;
            this.stream = stream;
            this.bookmarkStream = new MemoryStream(this.blocksize / 64);
            this.dataStream = new MemoryStream(this.blocksize);
            this.bookmarks = new Queue<uint>();
        }

        public void Read(Func<IBytelotChunk, bool> processor = null)
        {
            var read = true;
            while (read)
            {
                lock (stream)
                {
                    if (dirty)
                        Flush();

                    if (header == null || stream.Position % blocksize == 0)
                    {
                        var newheader = Header.ReadHeader(stream, blocksize, stream.Position);
                        Header.ValidateHeader(header, newheader);
                        header = newheader;
                        blocksize = header.LotBlockSize;
                    }

                    if (bookmarks.Count == 0)
                        foreach (var bookmark in ReadBookmarks(stream))
                            bookmarks.Enqueue(bookmark);

                    if (bookmarks.Count == 0)
                    {
                        read = false;
                    }
                    else
                    {
                        var len = bookmarks.Dequeue();
                        var dat = stream.Read(len);

                        read = processor != null && processor(new BlockBytelotChunk(dat, header.LotId.ToString(), readCount.ToString()));
                        readCount++;
                    }
                }
            }
        }

        protected static IEnumerable<uint> ReadBookmarks(Stream stream)
        {
            ulong bookmark;
            while (0 != (bookmark = stream.ReadCompactULongBytes().ULongFromCompactBytes()))
            {
                if (bookmark > uint.MaxValue) throw new InvalidDataException("bookmarks are uint");
                yield return (uint)bookmark;
            }
        }

        public IBytelotChunk Write(byte[] data = null)
        {
            if (!writeInitialized) WriteInit();

            if (data == null) return null;

            var lenbytes = ((ulong)data.LongLength).ULongToCompactBytes();

            lock (stream)
            {
                dirty = true;
                bookmarkStream.Write(lenbytes);
                dataStream.Write(data);

                var l = (stream.Position % blocksize) + bookmarkStream.Length + 1 + dataStream.Length;

                var chunk = new BlockBytelotChunk(data, header.LotId.ToString(), writeCount.ToString());

                writeCount++;

                if (l > blocksize)
                    Flush();

                return chunk;
            }
        }

        protected void WriteInit()
        {
            using (stream.Track(true))
            {
                var newheader = Header.ReadHeader(stream, blocksize, stream.Length);
                Header.ValidateHeader(header, newheader);
                var bookmarks = ReadBookmarks(stream);
                newheader.LotPosition = newheader.LotPosition + (uint)bookmarks.Count();
                if (header == null)
                {
                    newheader.LotId = Guid.NewGuid();
                    newheader.LotHeader = "MBLa";
                    newheader.LotBlockSize = blocksize;
                }
                header = newheader;
                writeCount = header.LotPosition;
                writeInitialized = true;
            }
        }


        protected void Flush()
        {
            using (stream.Track(true))
            {

                if (0 == (stream.Position % blocksize))
                {
                    stream.Write(header.AllBytes);
                }

                var split = dataStream.Length - ((stream.Position + bookmarkStream.Length + 1 + dataStream.Length) % blocksize);
                if (split <= 0) split = dataStream.Length;

                stream.Write(bookmarkStream, srcSeek: 0);
                stream.WriteByte(0);
                stream.Write(dataStream, srcSeek: 0, srcCount: split);
                if (dataStream.Length > split)
                {
                    var newblockpos = stream.Length + blocksize - stream.Length % blocksize;
                    var newheader = new Header(header.AllBytes)
                    {
                        LotPosition = writeCount,
                        LotOverlap = true
                    };
                    stream.Write(data: newheader.AllBytes, dstSeek: newblockpos);
                    stream.Write(dataStream);
                }
                dataStream.SetLength(0);
                bookmarkStream.SetLength(0);
                dirty = false;
            }

            stream.Flush();
        }

        class Header
        {
            public static Header ReadHeader(Stream stream, int blocksize, long position)
            {
                position -= (position % blocksize);
                stream.Seek(position, SeekOrigin.Begin);
                return new Header(stream);
            }

            public static void ValidateHeader(Header header, Header newheader)
            {
                if (header != null)
                {
                    if (newheader.LotHeader != header.LotHeader)
                        throw new InvalidDataException(String.Format("Expected lot header {0}, found {1}", header.LotHeader, newheader.LotHeader));
                    if (newheader.LotId != header.LotId && header.LotId != Guid.Empty)
                        throw new InvalidDataException(String.Format("Expected lot id {0}, found {1}", header.LotId, newheader.LotId));
                }
            }

            byte[] bytes;

            public Header(Stream stream)
            {
                bytes = ReadBytes(stream);
            }

            public Header(byte[] bytes)
            {
                this.bytes = bytes;
            }

            protected static byte[] ReadBytes(Stream stream)
            {
                var bytes = stream.Read(32);

                if (bytes != null && bytes.Length != 32)
                    throw new InvalidDataException("Header bytes must be 32");

                return bytes == null ? new byte[32] : bytes;
            }

            public byte[] AllBytes
            {
                get { return bytes.ToArray(); }
            }

            public String LotHeader
            {
                get
                {
                    return ASCIIEncoding.ASCII.GetString(LotHeaderBytes);
                }
                set
                {
                    var b = ASCIIEncoding.ASCII.GetBytes(value);
                    if (b.Length != 4) throw new InvalidDataException(String.Format("Expected 4 bytes, got {0}", b.Length));
                    Array.Copy(b, 0, bytes, 0, 4);
                }
            }
            public byte[] LotHeaderBytes
            {
                get
                {
                    var b = new byte[4];
                    Array.Copy(bytes, 0, b, 0, 4);
                    return b;
                }
            }
            public int LotBlockSize
            {
                get
                {
                    return (int)(LotBlockSizeBytes.UIntFromBigEndian());
                }
                set
                {
                    var b = ((uint)value).UIntToBigEndian();
                    Array.Copy(b, 0, bytes, 4, 4);
                }
            }
            public byte[] LotBlockSizeBytes
            {
                get
                {
                    var b = new byte[4];
                    Array.Copy(bytes, 4, b, 0, 4);
                    return b;
                }
            }
            public Guid LotId
            {
                get
                {
                    return new Guid(LotIdBytes);
                }
                set
                {
                    var b = value.ToByteArray();
                    Array.Copy(b, 0, bytes, 8, 16);
                }
            }
            public byte[] LotIdBytes
            {
                get
                {
                    var b = new byte[16];
                    Array.Copy(bytes, 8, b, 0, 16);
                    return b;
                }
            }
            public ulong LotPosition
            {
                get
                {
                    var pos = LotPositionBytes.ULongFromBigEndian();
                    pos = pos << 1;
                    pos = pos >> 1;
                    return pos;
                }
                set
                {
                    var max = ulong.MaxValue;
                    max = max << 1;
                    max = max >> 1;
                    if (value > max) throw new ArgumentOutOfRangeException(String.Format("Maximum: {0}, given: {1}", max, value));

                    if (LotOverlap)
                        value |= ((ulong)1) << 63;

                    var b = value.ULongToBigEndian();
                    Array.Copy(b, 0, bytes, 24, 8);
                }
            }
            public byte[] LotPositionBytes
            {
                get
                {
                    var b = new byte[8];
                    Array.Copy(bytes, 24, b, 0, 8);
                    return b;
                }
            }
            public bool LotOverlap
            {
                get
                {
                    return (bytes[24] & 0x80) != 0;
                }
                set
                {
                    if (value != LotOverlap)
                        bytes[24] = (byte)(bytes[24] ^ 0x80);
                }
            }

            public long Size { get { return bytes.Length; } }

        }
    }
}
