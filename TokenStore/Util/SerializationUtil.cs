using System;
using System.Linq;

namespace TokenStore.Util
{
    public static class SerializationUtil
    {
        public static byte[] ULongToCompactBytes(this ulong number)
        {
            if (number == 0) return new byte[] { (byte)0 };

            var bitcount = (byte)Math.Ceiling(Math.Log(number + 1, 2));
            if (bitcount == 0) bitcount = 64; // edge case of ulong max

            var bytecount = 1 + ((bitcount - 1) >> 3);
            var cbitcount = bitcount + bytecount;
            var cbytecount = 1 + ((cbitcount - 1) >> 3);

            var cbytes = new byte[cbytecount];

            for (var byteidx = 0; byteidx < cbytecount; byteidx++)
            {
                var lshift = (8 - cbytecount + byteidx);
                cbytes[byteidx] = (byte)((number << (8 * lshift)) >> 56);
            }

            cbytes[0] |= (byte)(((1 << (cbytecount - 1)) - 1) << (9 - cbytecount));

            return cbytes;
        }
        public static ulong ULongFromCompactBytes(this byte[] bytes)
        {
            if (bytes == null) return 0;

            ulong number = 0;
            int byteidx = 0;
            int length = 0;
            for (; byteidx < bytes.Length; byteidx++)
            {
                var b = bytes[byteidx];
                
                if (length == 0)
                {
                    length = CompactBytesLength(b);

                    b = (byte)((byte)(b << length) >> length);
                }

                number |= ((ulong)b) << (8 * (length - byteidx - 1));
            }

            if (byteidx != length) throw new InvalidOperationException(String.Format("Expected {0} bytes, got {1}.", length, byteidx));

            return number;
        }
        public static int CompactBytesLength(this byte firstByte)
        {
            int len;
            for (len = 0; len < 9; len++)
            {
                if (0 == (firstByte & (0x80 >> len))) break;
            }
            return len + 1;
        }

        public static byte[] ULongToBigEndian(this ulong number)
        {
            return (
                    BitConverter.IsLittleEndian
                    ? BitConverter.GetBytes(number).Reverse()
                    : BitConverter.GetBytes(number)
                    ).ToArray();
        }
        public static ulong ULongFromBigEndian(this byte[] eightbytes)
        {
            return BitConverter.IsLittleEndian
                    ? BitConverter.ToUInt64(eightbytes.Reverse().ToArray(), 0)
                    : BitConverter.ToUInt64(eightbytes, 0)
                    ;
        }
        public static byte[] UIntToBigEndian(this uint number)
        {
            return (
                    BitConverter.IsLittleEndian
                    ? BitConverter.GetBytes(number).Reverse()
                    : BitConverter.GetBytes(number)
                    ).ToArray();
        }
        public static uint UIntFromBigEndian(this byte[] eightbytes)
        {
            return BitConverter.IsLittleEndian
                    ? BitConverter.ToUInt32(eightbytes.Reverse().ToArray(), 0)
                    : BitConverter.ToUInt32(eightbytes, 0)
                    ;
        }



    }
}

