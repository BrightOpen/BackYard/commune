﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TokenStore.Util
{
    public static class Streams
    {
        public static int DefaultBufferSize = 4 * 1024;

        public static StreamPositionTracker Track(this Stream stream, bool rewindOnDispose)
        { return new StreamPositionTracker(stream, rewindOnDispose); }

        public static byte[] Read(this Stream srcStream, uint count)
        {
            if (count <= int.MaxValue) return srcStream.Read((int)count);

            var bytes1 = srcStream.Read(uint.MaxValue);
            var bytes2 = srcStream.Read(count - uint.MaxValue);

            return bytes1.Concat(bytes2).ToArray();
        }
        public static byte[] Read(this Stream srcStream, int count)
        {
            var buffer = new byte[count];
            var bytesread = srcStream.Read(buffer, 0, buffer.Length);
            if (bytesread == 0)
            {
                return null;
            }
            else
            {
                Array.Resize(ref buffer, bytesread);
                return buffer;
            }
        }

        public static byte[] ReadCompactULongBytes(this Stream srcStream)
        {
            byte[] compactBytes = null;
            int position = 0;
            do
            {
                var i = srcStream.ReadByte();

                if (i < 0)
                {
                    if (position == 0) return null;
                    else throw new InvalidDataException("Compact byte was incomplete");
                }

                var b = (byte)i;

                if (position == 0)
                {
                    compactBytes = new byte[b.CompactBytesLength()];
                    compactBytes[0] = b;
                    position++;
                }
                else
                {
                    compactBytes[position] = b;
                    position++;
                }
            } while (position < compactBytes.Length);
            return compactBytes;
        }

        public static void Write(this Stream dstStream, byte[] data, int dataOffset = -1, int dataCount = -1, int buffersize = 0, long dstSeek = -1, bool dstRewind = false)
        {
            if (dataOffset < 0) dataOffset = 0;
            if (dataCount < 0) dataCount = data.Length;

            var ms = dataOffset != 0 || dataCount != data.Length
                    ? new MemoryStream(data, dataOffset, dataCount)
                    : new MemoryStream(data, false)
                    ;

            dstStream.Write(ms, buffersize: buffersize, dstSeek: dstSeek, dstRewind: dstRewind);
        }

        public static void Write(this Stream dstStream, Stream srcStream, long srcSeek = -1, long srcCount = -1, bool srcRewind = false, int buffersize = -1, long dstSeek = -1, bool dstRewind = false)
        {
            if (dstStream == null) throw new ArgumentNullException("dstStream");
            if (!dstStream.CanWrite) throw new InvalidOperationException("dstStream not writable");

            if (srcStream == null) throw new ArgumentNullException("srcStream");
            if (!srcStream.CanRead) throw new InvalidOperationException("srcStream not readable");

            if (srcSeek < 0) srcSeek = srcStream.Position;
            if (srcCount < 0) srcCount = srcStream.Length - srcSeek;
            if (buffersize <= 0) buffersize = DefaultBufferSize;
            if (dstSeek < 0) dstSeek = dstStream.Position;

            var srcPosition = srcStream.Position;
            var dstPosition = dstStream.Position;

            if (srcSeek != srcPosition)
            {
                if (!srcStream.CanSeek)
                    throw new InvalidOperationException("srcStream does not support seeking");
                else
                    srcStream.Seek(srcSeek, SeekOrigin.Begin);
            }

            if (dstSeek != dstPosition)
            {
                if (!dstStream.CanSeek)
                    throw new InvalidOperationException("dstStream does not support seeking");
                else
                {
                    if (dstSeek > dstStream.Length)
                        dstStream.SetLength(dstSeek);

                    dstStream.Seek(dstSeek, SeekOrigin.Begin);
                }
            }
            
            using (srcStream.Track(srcRewind))
            using (dstStream.Track(dstRewind))
            {
                var buffer = new byte[buffersize > srcCount ? srcCount : buffersize];
                var bytesreadtotal = 0L;
                while (buffer.Length != 0L)
                {
                    var bytesread = srcStream.Read(buffer, 0, buffer.Length);

                    if (bytesread == 0) break;

                    dstStream.Write(buffer, 0, bytesread);

                    bytesreadtotal += bytesread;

                    if (bytesreadtotal + buffer.Length > srcCount)
                        buffer = new byte[srcCount - bytesreadtotal];
                }
            }
        }
    }
}
