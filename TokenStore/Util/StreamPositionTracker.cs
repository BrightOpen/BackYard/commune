﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TokenStore.Util
{
    public class StreamPositionTracker : IDisposable
    {
        Stream stream;
        long savedPosition;

        public StreamPositionTracker(Stream stream, bool rewindOnDispose)
        {
            if (rewindOnDispose && !stream.CanSeek)
                throw new InvalidOperationException("stream does not support seeking and rewind is required");

            this.stream = stream;

            if (rewindOnDispose)
                this.savedPosition = stream.Position;
            else
                this.savedPosition = -1;
        }

        public void Dispose()
        {
            if (savedPosition >= 0 && savedPosition != stream.Position)
            {
                if (!stream.CanSeek)
                    throw new InvalidOperationException("stream does not support seeking and rewind is required");

                stream.Seek(savedPosition, SeekOrigin.Begin);
            }
        }
    }
}
