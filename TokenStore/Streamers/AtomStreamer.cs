using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Text;
using TokenStore.Util;

namespace TokenStore.Streamers
{
	public class AtomStreamer
	{
		ulong MaxAtomIdx = 0;
		SortedList<string,byte[]> AtomDictionary;
		Stream AtomStream;
		
		public AtomStreamer (Stream atomStream)
		{
			AtomStream = atomStream;
			AtomDictionary = new SortedList<string, byte[]> ();
		}
		
		public byte[] GetAtomStreamCode (string atom)
		{			
			byte[] atomcode;
			
			if (!AtomDictionary.TryGetValue (atom, out atomcode)) {
				String readatom;
				while (ReadAtom(AtomStream, out atomcode, out readatom)) {
					var atomidx = atomcode.ULongFromCompactBytes ();
					if (atomidx > MaxAtomIdx)
						MaxAtomIdx = atomidx;
					
					AtomDictionary.Add (readatom, atomcode);
					
					if (atom == readatom)
						return atomcode;
				}
				
				atomcode = (MaxAtomIdx++).ULongToCompactBytes ();				
				AtomDictionary.Add (atom, atomcode);
				WriteAtom (AtomStream, atomcode, atom);
			}
		
			return atomcode;
		}
		
		public static void WriteAtom (Stream atomStream, byte[] atomcode, string atom)
		{
			var atomBytes = Encoding.UTF8.GetBytes (atom);			
			var bytes = atomcode
						.Concat (((ulong)atomBytes.Length).ULongToCompactBytes ())
						.Concat (atomBytes)
						.ToArray ();
			
			atomStream.Seek (0, SeekOrigin.End);
			atomStream.Write (bytes, 0, bytes.Length);
			atomStream.Flush ();
		}
		
		public static bool ReadAtom (Stream atomStream, out byte[] atomcode, out string atom)
		{
			if (atomStream.Length == atomStream.Position)
			{
				atomcode = null;
				atom = null;
				return false;	
			}
			
			atomcode = ReadCompactBytes(atomStream);
			atom = ReadString(atomStream);
			
			return true;
		}
			
		public static string ReadString (Stream atomStream)
		{			
			var len = (int) ReadCompactBytes(atomStream).ULongFromCompactBytes();
			
			var buff = new byte[len];
			var lenread=atomStream.Read(buff, 0, len);
			if(len!=lenread)
			{
				throw new InvalidDataException(String.Format("Expected {0} bytes, got {1}.",len,lenread));
			}
			return Encoding.UTF8.GetString(buff);
		}
		
		public static byte[] ReadCompactBytes (Stream atomStream)
		{			
			byte[] compactBytes = null;
			int position = 0;
			do {
				var b = (byte)atomStream.ReadByte ();
				
				if (position == 0) {
					compactBytes = new byte[b.CompactBytesLength ()];
					compactBytes [0] = b;
					position++;
				} else {
					compactBytes [position] = b;
					position++;
				}
			} while(position < compactBytes.Length);	
			return compactBytes;
		}
	}
}

