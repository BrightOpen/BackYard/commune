using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace TokenStore
{
	public class TokenWriter : ITokenProcessor
	{
		Stream TokenStream;
		Stream DataStream;
		Stream ReferenceStream;
		
		IDictionary<string, ITokenSerializer[]> Serializers;
		
		
		public TokenWriter (Stream tokenStream, Stream dataStream, Stream referenceStream, params ITokenSerializer[] serializers)
		{
			if(tokenStream==null) throw new ArgumentNullException("tokenStream");
			if(dataStream==null) throw new ArgumentNullException("dataStream");
			if(referenceStream==null) throw new ArgumentNullException("referenceStream");
			if(serializers==null) throw new ArgumentNullException("serializers");			
			
			if(!tokenStream.CanWrite) throw new InvalidOperationException("tokenStream is not writable");
			if(!dataStream.CanWrite) throw new InvalidOperationException("dataStream is not writable");
			if(!referenceStream.CanWrite) throw new InvalidOperationException("referenceStream is not writable");
			
			TokenStream = tokenStream;
			DataStream = dataStream;
			ReferenceStream = referenceStream;
			
			Serializers = (
				from s in serializers
				from t in s.SerializedTypes
				select Tuple.Create(t, s)
				).GroupBy(pair=>pair.Item1)
				.ToDictionary(g=>g.Key,g=>g.Select(pair=>pair.Item2).ToArray());
		}
	
		
		#region ITokenProcessor implementation
		public void Process<T> (T token) where T : IToken
		{
			if(!Serializers.ContainsKey(token.Type))
				throw new InvalidOperationException(String.Format("Unsupported token type {0}",token.Type));
			
			
			
			using(var tokenMemStream = new MemoryStream())
			using(var tokenMemWriter = new StreamWriter( tokenMemStream))
			using(var atomMemStream = new MemoryStream())
			using(var atomMemWriter = new StreamWriter( atomMemStream))
			using(var dataMemStream = new MemoryStream())
			using(var dataMemWriter = new StreamWriter( dataMemStream))
			{
				bool serialized = false;
				foreach(var serializer in Serializers[token.Type])
				{
					if(serializer.Serialize(token, tokenMemWriter))
					{
						serialized = true;
						break;
					}
				}
				
				if(!serialized) throw new InvalidOperationException(String.Format("Couldn't serialize token type {0}", token.Type));
				
				WriteType(token, dataMemWriter, atomMemWriter);
			};
		}
		#endregion
				                        
		protected void WriteType(IToken token, StreamWriter dataWriter, StreamWriter atomWriter)
		{
			
			
			//dataWriter.Write(typecode);
		}
		
	}
}

