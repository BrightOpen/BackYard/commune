using System;
using System.IO;

namespace TokenStore
{
	public interface ITokenSerializer
	{
		String[] SerializedTypes {get;}
		
		bool Serialize (IToken token, StreamWriter writer);
	}
}

