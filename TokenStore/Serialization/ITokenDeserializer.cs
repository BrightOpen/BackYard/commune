using System;
using System.IO;

namespace TokenStore
{
	public interface ITokenDeserializer
	{
		String[] DeserializedTypes {get;}
		
		IToken Deserialize(String type, StreamReader reader);
	}
}

